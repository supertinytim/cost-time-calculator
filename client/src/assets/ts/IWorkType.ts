export default interface IWorkType {
  /** Identifier of the rate */
  id: number;
  /** Label describing the rate type */
  label: string;
  /** The hourly rate in dollars */
  rate: number;
}
