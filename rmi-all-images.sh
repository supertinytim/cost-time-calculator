# (Forcefully) Remove all images from docker that pattern mathc phrase

if [ $# -eq 0 ]
  then
    printf "\n\"$0\" requires at least 1 argument.\n\n"
    printf "Usage: rmi-all-images IMAGE [IMAGE...]\n\n"
fi

for var in "$@"
do
    docker images | grep $var | tr -s ' ' | cut -d ' ' -f 2 | xargs -I {} docker rmi -f $var:{}
done
