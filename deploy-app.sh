#!/bin/bash

# This is to be ran on the server to deploy the most recent version of the code

# NOTE: The .env file in server SHOULD be copied over and edited from .env.example prior

if [ ! -f ./server/.env ]; then
    printf "\n\".env\" does not exist in server."
    printf "\n\nCopy \".env.example\" and edit it's values before proceeding.\n\n"
    exit 1
fi

echo -n "Mysql Password: "
read -s MYSQL_PASSWORD

export MYSQL_PASSWORD

# Set git hash variable for pulling proper image
export CTC_GIT_SHA="$(git log -1 --pretty=%H)"

# Pull in the newer images
docker-compose pull nginx php

# Restart the containers
docker-compose -f docker-compose.yml up -d