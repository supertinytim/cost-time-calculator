FROM nginx

COPY ./client/dist /usr/share/nginx/html
COPY ./default.conf /etc/nginx/conf.d/default.conf