<?php

use Illuminate\Database\Seeder;
use App\WorkType;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        WorkType::create(['label' => 'Development', 'rate' => 160]);
        WorkType::create(['label' => 'Design', 'rate' => 120]);
        WorkType::create(['label' => 'Project Management', 'rate' => 120]);
        WorkType::create(['label' => 'Travel', 'rate' => 100]);
    }
}
