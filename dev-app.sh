#!/bin/bash

# Run this to create a dev version of the application
if [ $# -eq 0 ] || [ $1 != "--no-build" ]; then
	cd client
	npm install
	npm run build
	cd ..
fi

echo -n "Host computer IP address: "
read PHP_IDE_CONFIG

export PHP_IDE_CONFIG

# Instead of using the git SHA. This is so that upon git updates, there aren't many of the same image
export CTC_GIT_SHA="dev"

# Dev password
export MYSQL_PASSWORD="jell-ctc-temp-pw"

docker-compose -f docker-compose.yml build php

docker-compose build php

# Restart the containers
docker-compose up